import express from "express";
import {
  resolveToWalletAddress,
  getParsedNftAccountsByOwner,
} from "@nfteyez/sol-rayz";
const web3 = require("@solana/web3.js");


function main() {
  const app = express();
  const port = 8080;
  let connection = new web3.Connection(web3.clusterApiUrl('devnet'));
  app.get("/listNft", async (req: any, res: any) => {
    const address = (req.query.address);
    // const address = "NftEyez.sol";

    const publicAddress = await resolveToWalletAddress({
      text: address
    });

    const nftArray = await getParsedNftAccountsByOwner({
      publicAddress,
      connection
    });
    console.log(nftArray);
    
    res.send(nftArray)
  }
  );

  app.listen(port, () => {
    console.log(`[server] server dimulai di http://localhost:${port} ⚡`);
  });
}
main();
